# IMS API Server

This project was generated with [Visual Studio 2022](https://visualstudio.microsoft.com).

## How to run this project

Step1 :  Open the file xxx\ims\backend\IMS.Backend.sln by Visual Studio, or, Open the folder xxx\ims\backend\ by VS Code  
Step2 :  Run or debug the program  
Step3 :  Access the link http://localhost:5091/swagger and you can see the RESTful API document  